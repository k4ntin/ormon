const Engine = require("./engine")

class Model {
  constructor(state = {}) {
    this._col = Engine.mod2col(this.constructor.name)
    this.$state = Object.assign({}, state)
  }

  static async import(data, filter = a => a) {
    const promises = []

    for (const item of data) {
      const model = new this(filter(item))
      promises.push(model.save())
    }

    return Promise.all(promises)
  }

  async save() {
    const inserted = await Engine.Driver.insertOne(this._col, this.$state)
    this.$state._id = inserted.insertedId
  }

  static async get(query = {}, opts = {}) {
    const col = Engine.mod2col(this.name)
    const data = await Engine.Driver.findOne(col, query, opts)
    if (data) return new this(data)
    return null
  }

  static async getAll({query = {}, filter = a => a, format = a => a}) {
    const col = Engine.mod2col(this.name)
    const data = await Engine.Driver.findMany(col, query)
    return filter(data).map(d => format(new this(d))).toArray()
  }

  format() {
    const data = Object.assign({}, this.$state)
    delete data._id
    return data
  }
}

module.exports = Model