const mongo = require("mongodb").MongoClient

class Mongo {
  constructor(uri, opts) {
    this._uri = uri
    this._opts = Object.assign({
      useNewUrlParser: true
    }, opts)
    this._client = null
    this._db = null
  }

  async connect() {
    this._client = await mongo.connect(this._uri, this._opts)
    this._db = this._client.db()
  }

  async close() {
    if (!this._db) return
    const op = this._client.close()
    this._client = null
    this._db = null
    return op
  }

  async findOne(collection, query, opts) {
    if (!this._db) await this.connect()
    return this._db.collection(collection).findOne(query, opts)
  }

  async findMany(collection, query, opts) {
    if (!this._db) await this.connect()
    return this._db.collection(collection).find(query, opts)
  }

  async insertOne(collection, document, opts) {
    if (!this._db) await this.connect()
    return this._db.collection(collection).insertOne(document, opts)
  }

  async insertMany(collection, document, opts) {
    if (!this._db) await this.connect()
    return this._db.collection(collection).insertMany(document, opts)
  }
}

module.exports = Mongo