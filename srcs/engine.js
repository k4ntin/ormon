let __driver = null

const __col2mod = {}
const __mod2col = {}

class Engine {
  static init(driver) {
    __driver = driver
  }

  static col2mod(col) { return __col2mod[col] }
  static mod2col(mod) { return __mod2col[mod] }

  static Bind(collection, model) {
    __col2mod[collection] = model
    __mod2col[model.name] = collection
  }

  static get Driver() { return __driver }
}

module.exports = Engine